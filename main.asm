%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_SIZE 256
%define NO_ERRORS_EXIT_CODE 0
%define READ_ERROR_EXIT_CODE 1
%define FIND_WORD_ERROR_EXIT_CODE 2

section .bss
    buffer: resb BUF_SIZE


section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUF_SIZE
    call read_string
    test rax, rax
    jz .error_buffer_full

    mov rdi, buffer
    mov rsi, DICTIONARY_START
    call find_word
    test rax, rax
    jz .error_word_not_found

    mov rdi, rax
    call print_string

    mov rdi, NO_ERRORS_EXIT_CODE
    jmp exit

    .error_buffer_full:
        mov rdi, READ_ERROR_EXIT_CODE
        jmp exit

    .error_word_not_found:
        mov rdi, FIND_WORD_ERROR_EXIT_CODE
        jmp exit