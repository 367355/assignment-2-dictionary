ASM=nasm
ASMFLAGS=-g -f elf64
LD=ld

MAIN=main

.PHONY: run clean test

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

all: clean $(MAIN) run

main.o: main.asm words.inc colon.inc dict.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ main.asm

$(MAIN): $(MAIN).o lib.o dict.o
	$(LD) -o $@ $^

test:
	python3 test.py

clean:
	rm -rf *.o
	rm -rf $(MAIN)

run: $(MAIN)
	./$<
