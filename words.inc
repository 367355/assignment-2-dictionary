
%ifndef WORDS_INC
%define WORDS_INC

%include "colon.inc"


section .data
%define DICTIONARY_START first

colon "third word", third
db "value 3", 0

colon "second word", second
db "value 2", 0

colon "first word", DICTIONARY_START
db "value 1", 0


%endif
