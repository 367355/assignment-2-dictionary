#!/usr/bin/python

import unittest
import subprocess
from subprocess import CalledProcessError, PIPE, Popen

WORDS_INC_PATTERN = """
%ifndef WORDS_INC
%define WORDS_INC

%include "colon.inc"

{}

%endif
"""

NOT_EMPTY_DICTIONARY_EXAMPLE = """
section .data
%define DICTIONARY_START first

colon "third word", third
db "value 3", 0

colon "second word", second
db "value 2", 0

colon "first word", DICTIONARY_START
db "value 1", 0
"""

EMPTY_DICTIONARY_EXAMPLE = """
    %define DICTIONARY_START 0
"""


class TestFindWordFunction(unittest.TestCase):
    def make(self, target: str):
        self.assertEqual(subprocess.call( ['make', target] ), 0)


    def launch(self, fname, input):
        output = b''
        try:
            p = Popen(['./'+fname], shell=None, stdin=PIPE, stdout=PIPE)
            (output, _) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (output.decode(), p.returncode)
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)


    def test_word_in_dicionary(self):
        inputsWithOutputs = {
            "first word": "value 1",
            "second word": "value 2",
            "third word": "value 3"
        }

        with open("words.inc", "w") as file:
            file.write(WORDS_INC_PATTERN.format(NOT_EMPTY_DICTIONARY_EXAMPLE))

        self.make("main")
        for key, value in inputsWithOutputs.items():
            (output, code) = self.launch("main", key)
            self.assertEqual(output, value, "wrong output for key {}".format(key))
            self.assertEqual(code, 0, "wrong exit code for key {}".format(key))


    def test_word_not_in_dictionary(self):
        inputs = ["fourth word", ""]

        with open("words.inc", "w") as file:
            file.write(WORDS_INC_PATTERN.format(NOT_EMPTY_DICTIONARY_EXAMPLE))

        self.make("main")
        for word in inputs:
            (output, code) = self.launch("main", word)
            self.assertEqual(output, "", "wrong output for key {}".format(word))
            self.assertEqual(code, 2, "wrong exit code for key {}".format(word))


    def test_empty_dictionary(self):
        input_word = "just a key"

        with open("words.inc", "w") as file:
            file.write(WORDS_INC_PATTERN.format(EMPTY_DICTIONARY_EXAMPLE))

        self.make("main")
        (output, code) = self.launch("main", input_word)

        self.assertEqual(output, "", "wrong output for key {}".format(input_word))
        self.assertEqual(code, 2, "wrong exit code for key {}".format(input_word))


    def test_too_long_key(self):
        too_long_key = "a" * 256 # считая завершающий \0 получается 256 символов

        with open("words.inc", "w") as file:
            file.write(WORDS_INC_PATTERN.format(NOT_EMPTY_DICTIONARY_EXAMPLE))

        self.make("main")
        (output, code) = self.launch("main", too_long_key)

        self.assertEqual(output, "", "wrong output for key {}".format(too_long_key))
        self.assertEqual(code, 1, "wrong exit code for key {}".format(too_long_key))


if __name__ == "__main__":
    unittest.main()
