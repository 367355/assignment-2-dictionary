%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define STD_IN 0
%define STD_OUT 1

section .text
; globals
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], `\0`
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    mov rsi, rsp
    mov r10, 10
    dec rsi
    sub rsp, 21
    mov rax, rdi
    mov byte [rsi], `\0`
    .loop:
        xor rdx, rdx
        div r10
        add rdx, `0`
        dec rsi
        mov byte [rsi], dl
        test rax, rax
        jnz .loop
    .end:
        mov rdi, rsi
        call print_string
        add rsp, 21
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, `-`
    call print_char
    pop rdi
    neg rdi
    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov r10b, [rsi + rax]
        cmp byte [rdi + rax], r10b
        jne .false_end
        cmp byte [rdi + rax], `\0`
        je .true_end
        inc rax
        jmp .loop
    .false_end:
        xor rax, rax
        ret
    .true_end:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    dec rsp
    mov byte [rsp], 0
    mov rdx, 1
    mov rsi, rsp
    xor rdi, rdi
    syscall
    mov al, byte [rsp]
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    xor r13, r13
    mov r14, rsi
    .loop:
        call read_char
        cmp r13, r14
        jae .error
        cmp al, ` `
        je .space
        cmp al, `\t`
        je .space
        cmp al, `\n`
        je .space
        cmp al, `\0`
        je .end
        mov byte [r12 + r13], al
        inc r13
        jmp .loop
    .space:
        test r13, r13
        jz .loop
    .end:
        mov byte [r12 + r13], `\0`
        mov rax, r12
        mov rdx, r13
    .return:
        pop r14
        pop r13
        pop r12
        ret
    .error:
        xor rax, rax
        jmp .return

read_string:
    push r12
    push r13
    push r14
    push rdi
    mov r12, rdi
    xor r13, r13
    mov r14, rsi
    .loop:
        cmp r13, r14
        jae .error
        call read_char
        cmp al, `\n`
        je .end
        cmp al, `\0`
        je .end
        mov byte [r12], al
        inc r13
        inc r12
        jmp .loop
    .end:
        mov byte [r12], `\0`
        mov rdx, r13
        pop rax
    .return:
        pop r14
        pop r13
        pop r12
        ret
    .error:
        pop rax
        xor rax, rax
        jmp .return

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r10, 10
    xor r11, r11
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp byte [rdi + rcx], `0`
        jb .end
        cmp byte [rdi + rcx], `9`
        ja .end
        mul r10
        mov r11b, byte [rdi + rcx]
        sub r11b, `0`
        add rax, r11
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], `+`
    je .positive
    cmp byte [rdi], `-`
    je .negative
    jmp parse_uint
    .positive:
        inc rdi
        jmp parse_uint
    .negative:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .error
        inc rdx
        neg rax
    .error:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        je .error
        mov r10b, byte [rdi + rcx]
        mov byte [rsi + rcx], r10b
        inc rcx
        cmp r10b, `\0`
        je .end
        jmp .loop
    .end:
        mov rax, rcx
    .error:
        ret
