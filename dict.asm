%define ADDRESS_LEN 8

section .text

%include "lib.inc"

global find_word

; rdi(r12) - строка, rsi(r13) - список
find_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    .loop:
        test r13, r13
        jz .not_found
        mov r14, qword[r13]
        add r13, ADDRESS_LEN
        mov rdi, r12
        mov rsi, r13
        call string_equals
        test rax, rax
        jnz .found
        mov r13, r14
        jmp .loop
    .found:
        mov rdi, r13
        call string_length
        add rax, r13
        inc rax
        jmp .end
    .not_found:
        xor rax, rax
    .end:
        pop r14
        pop r13
        pop r12
        ret
        
