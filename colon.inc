%ifndef COLON_INC
%define COLON_INC

%xdefine next_pointer 0

%macro colon 2
    %2:
    dq next_pointer
    db %1, 0
    %xdefine next_pointer %2
%endmacro
%endif
